﻿using System;
using System.Collections.Generic;

namespace LinkHomeworkEx1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> words = new List<string>();
            words.Add("dog");
            words.Add("cat");

            string joinedWords = string.Join(", ", words);

            Console.WriteLine(joinedWords);
        }
    }
}
