﻿using System;
using System.Collections.Generic;

namespace LinkHomeworkEx2
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] cities_names = { "ROME", "LONDON", "NAIROBI", "CALIFORNIA", "ZURICH", "NEW DELHI", "AMSTERDAM", "ABU DHABI", "PARIS" };
            List<string> cities = new List<string>();
            cities.AddRange(cities_names);

            char start = 'A';
            char end = 'M';

            string result = cities.Find(s => s.StartsWith(start) && s.EndsWith(end));
            Console.WriteLine($"The city starting with {start} and ending with {end} is {result}");
        }
    }
}
