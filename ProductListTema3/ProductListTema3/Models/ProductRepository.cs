﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductListTema3.Models
{
    public class ProductRepository
    {
        private List<Product> _products = new List<Product>
        {
            new Product
            {
                ID=1,
                Name="Ciocolata",
                Description="Cu lapte"
            },

            new Product
            {
                ID=2,
                Name="Lapte",
                Description="1.5% Grasime"
            }
        };

        public List<Product> Products => _products;
    }
}
