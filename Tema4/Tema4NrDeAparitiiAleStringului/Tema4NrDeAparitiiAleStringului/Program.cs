﻿using System;
using System.IO;

namespace Tema4NrDeAparitiiAleStringului
{
    class Program
    {

        public static int NumberOfOccurences(string inputString, Stream stream)
        {
            if (string.IsNullOrEmpty(inputString))
            {
                throw new ArgumentNullException(nameof(inputString));
            }

            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            if (!stream.CanRead)
            {
                throw new ArgumentException("Stream cannot be read");
            }

            // add your implementation here.
            string line;
            string stringToSplit = "abc";
            int counter = 0;

            StreamReader streamReader = new StreamReader(stream);

            while (!streamReader.EndOfStream)
            {
                line = streamReader.ReadLine();
                int countPerLine = line.Split(stringToSplit).Length - 1;
                counter += countPerLine;
            }

            stream.Close();

            return counter;
        }


        static void Main(string[] args)
        {
            var stringToFind = "abc";
            using (StreamReader sr = new StreamReader("../../../Text.data"))
            {
                Console.WriteLine(NumberOfOccurences(stringToFind, sr.BaseStream));
            }
        }
    }
}
