﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Tema4MoveAllZeroToEnd
{
    class Program
    {

        public static void Swap<T>(T lhs, T rhs)
        {
            T temp = lhs;
            lhs = rhs;
            rhs = temp;
        }

        static void Main(string[] args)
        {

            StreamReader sr = new StreamReader("../../../Array.txt");

            string line = sr.ReadLine();

            string[] elements = line.Split(" ");
            List<int> array = new List<int>();

            foreach (var item in elements)
            {
                array.Add(int.Parse(item));
            }

            int count = 0;

            for (int i = 0; i < array.Count; i++)
            {
                if (array[i] != 0)
                {
                    (array[count], array[i]) = (array[i], array[count]);
                    count++;
                }
            }

            array.ForEach(item => Console.Write(item));

        }
    }
}
