﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Tema4_Interclasare_Vectori
{
    class Program
    {
        public static List<int> fromStringArrayToList(string[] numbers)
        {
            List<int> intNumbers = new List<int>();

            foreach (var item in numbers)
            {
                intNumbers.Add(int.Parse(item));
            }

            return intNumbers;
        }
        static void Main(string[] args)
        {
            List<int> firstArray = new List<int>();
            List<int> secondArray = new List<int>();
            try
            {
                using (StreamReader sr = new StreamReader("../../../Arrays.txt"))
                {
                    string line;

                    line = sr.ReadLine();
                    var numbers = line.Split(" ");

                    firstArray = fromStringArrayToList(numbers);

                    line = sr.ReadLine();
                    numbers = line.Split(" ");
                    secondArray = fromStringArrayToList(numbers);

                }
            }
            catch (Exception e)
            {
                Console.WriteLine("The file could not be read:");
                Console.WriteLine(e.Message);
            }

            int i = 0;
            int j = 0;

            List<int> result = new List<int>();

            while (i < firstArray.Count && j < secondArray.Count)
            {
                if (firstArray[i] <= secondArray[j])
                {
                    result.Add(firstArray[i]);
                    ++i;
                }
                else
                {
                    result.Add(secondArray[j]);
                    ++j;
                }

            }

            while (i < firstArray.Count)
            {
                result.Add(firstArray[i]);
                ++i;
            }

            while (j < secondArray.Count)
            {
                result.Add(secondArray[j]);
                ++j;
            }

            result.ForEach(item => Console.Write(item));


            
        }
    }
}
