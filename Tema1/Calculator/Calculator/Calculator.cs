﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    class Calculator
    {
        public int Add(int x, int y) => x + y;

        public int Diff(int x, int y) => x - y;

        public int Multiply(int x, int y) => x * y;

        public int Divide(int x, int y) => x / y;
    }
}
