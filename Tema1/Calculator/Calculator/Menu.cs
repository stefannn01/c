﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    class Menu
    {

        Calculator calculator;
        public Menu()
        {
            calculator = new Calculator();
        }

        private static int[] readData()
        {

            int[] parameters = new int[2];

            Console.WriteLine("First parameter: ");
            int.TryParse(Console.ReadLine(), out parameters[0]);
            Console.WriteLine("Second parameter: ");
            int.TryParse(Console.ReadLine(), out parameters[1]);

            return parameters;
        }

        public void start()
        {
            while (true)
            {
                Console.WriteLine("0. EXIT ");
                Console.WriteLine("1. ADD");
                Console.WriteLine("2. DIFF");
                Console.WriteLine("3. MULTIPLY");
                Console.WriteLine("4. DIVIDE");

                int choice = 0;
                int.TryParse(Console.ReadLine(), out choice);

                switch (choice)
                {
                    case 0:
                        {
                            return;
                        }
                    case 1:
                        {
                            int[] parameters = readData();

                            Console.WriteLine($"Result: {calculator.Add(parameters[0], parameters[1])}");
                            Console.ReadLine();
                            break;
                        }
                    case 2:
                        {
                            int[] parameters = readData();
                            Console.WriteLine($"Result: {calculator.Diff(parameters[0], parameters[1])}");
                            Console.ReadLine();

                            break;
                        }
                    case 3:
                        {
                            int[] parameters = readData();
                            Console.WriteLine($"Result: {calculator.Multiply(parameters[0], parameters[1])}");
                            Console.ReadLine();
                            break;
                        }
                    case 4:
                        {
                            int[] parameters = readData();
                            Console.WriteLine($"Result: {calculator.Divide(parameters[0], parameters[1])}");
                            Console.ReadLine();
                            break;
                        }
                    default:
                        break;


                }
                Console.Clear();
            }
        }
    }
}
