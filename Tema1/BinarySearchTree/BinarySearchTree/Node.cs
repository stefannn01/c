﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTree
{
    class Node
    {
        public Node(int value)
        {
            this.Value = value;
            this.LeftChild = null;
            this.RightChild = null;
        }

        public int Value { get; set; }
        public Node LeftChild { get; set; }
        public Node RightChild { get; set; }


        public override string ToString()
        {
            return this.Value.ToString();
        }
    }
}
