﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BinarySearchTree
{
    class Tree
    {
        public Node Root { get; set; }

        public Tree()
        {
            Root = null;
        }


        public void Insert(int value)
        {
            Node node = new Node(value);

            if (Root == null)
            {
                Root = node;
                return;
            }
            else
            {
                Node parent = null;
                Node current = Root;

                while (current != null)
                {
                    parent = current;

                    if (value > current.Value)
                    {
                        current = current.RightChild;
                    }
                    else if (value < current.Value)
                    {
                        current = current.LeftChild;
                    }
                }

                if (value < parent.Value)
                {
                    parent.LeftChild = node;
                }
                else
                {
                    parent.RightChild = node;
                }
            }
        }


        public void InorderTraversalIterative()
        {
            Stack<Node> stack = new Stack<Node>();
            Node current = Root;

            while (stack.Count != 0 || current != null)
            {
                if (current != null)
                {
                    stack.Push(current);
                    current = current.LeftChild;
                }
                else
                {
                    current = stack.Pop();
                    Console.Write(current.Value + " ");
                    current = current.RightChild;
                }
            }
        }


        public void PreOrderTraversalIterative()
        {
            if (Root == null)
            {
                return;
            }

            Stack<Node> stack = new Stack<Node>();
            stack.Push(Root);

            while(stack.Count!=0)
            {
                Node node = stack.Pop();
                Console.Write(node.Value+" ");


                if(node.RightChild!=null)
                {
                    stack.Push(node.RightChild);
                }

                if(node.LeftChild!=null)
                {
                    stack.Push(node.LeftChild);
                }
            }
        }


        public void PostOrderTraversalRecursive(Node node)
        {
            if (node == null)
            {
                return;
            }
            PostOrderTraversalRecursive(node.LeftChild);
            PostOrderTraversalRecursive(node.RightChild);
            Console.Write(node.Value+" ");
        }
        public void PreOrderTraversalRecursive(Node node)
        {
            if(node==null)
            {
                return;
            }
            Console.Write(node.Value+" ");
            PreOrderTraversalRecursive(node.LeftChild);
            PreOrderTraversalRecursive(node.RightChild);
        }

        public void InOrderTraversalRecursive(Node node)
        {
            if(node==null)
            {
                return;
            }
            InOrderTraversalRecursive(node.LeftChild);
            Console.Write(node.Value+" ");
            InOrderTraversalRecursive(node.RightChild);
        }

        public void Minimum()
        {
            Node current = Root;
            while(current.LeftChild!=null)
            {
                current = current.LeftChild;
            }
            Console.WriteLine(current.Value);
        }

        public void Maximum()
        {
            Node current = Root;
            while(current.RightChild!=null)
            {
                current = current.RightChild;
            }
            Console.WriteLine(current.Value);
        }

        public Node Search(int value)
        {
            Node current = Root;

            while(current!=null)
            {
                if(current.Value==value)
                {
                    return current;
                }
                else
                {
                    if(value<current.Value)
                    {
                        current = current.LeftChild;
                    }
                    else
                    {
                        current = current.RightChild;
                    }
                }
            }
            return null;
        }


        public void Delete(int value)
        {
            // Node node = Search(value);

            Node current = Root;
            Node node=null;
            while (current != null)
            {
                if (current.Value == value)
                {
                    node = current;
                }
                else
                {
                    if (value < current.Value)
                    {
                        current = current.LeftChild;
                    }
                    else
                    {
                        current = current.RightChild;
                    }
                }
            }
            //case 1, there are no subtrees
            if (node.LeftChild==null && node.RightChild==null)
            {
                if(node.Value==Root.Value)
                {
                    Root = null;
                    return;
                }

                node = null;

            }
        }
    }

}
