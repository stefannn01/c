﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema1
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }

        public override string ToString()
        {
            return $"Fisrt Name:{FirstName}, Last Name: {LastName}";
        }

    }
}
