﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Tema1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student s1 = new Student
            {
                FirstName = "Mares",
                LastName = "Stefan",
                Address = "adresa_s1",
                Medie = 10.0d,
                NumarMAtricol = "301"
            };

            Student s2 = new Student
            {
                FirstName = "Alex",
                LastName = "Radu",
                Address = "adresa_s2",
                Medie = 10.0d,
                NumarMAtricol = "301"
            };

            Person p1 = new Person
            {
                FirstName = "Mihai",
                LastName = "Doru",
                Address = "adresa_p1",
            };

            Person p2 = new Person
            {
                FirstName = "Marian",
                LastName = "Dumitru",
                Address = "adresa_p2",

            };

            List<Person> personList = new List<Person>
            {
                s1,s2,p1,p2
            };

            Console.WriteLine("Sort First Name");
            personList.Sort((pers1, pers2)=> pers1.FirstName.CompareTo(pers2.FirstName));
            personList.ForEach(Console.WriteLine);

            Console.WriteLine("Sort Last Name");
            personList.Sort((pers1, pers2) => pers1.LastName.CompareTo(pers2.LastName));
            personList.ForEach(Console.WriteLine);

        }
    }
}
