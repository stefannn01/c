﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema1
{

    enum GradProfesor
    {
        definitivat,
        grad_1,
        grad_2,
        doctorat
    }

    class Teacher:Person
    {
        public GradProfesor Grad { get; set; }
    }
}
