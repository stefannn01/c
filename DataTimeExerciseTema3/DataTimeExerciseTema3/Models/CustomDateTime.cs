﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataTimeExerciseTema3.Models
{
    public class CustomDateTime
    {
        public DateTime DateTime
        {
            get
            {
                return DateTime.Now;
            }
        }
    }
}
