﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataTimeExerciseTema3.Models;
using Microsoft.AspNetCore.Mvc;

namespace DataTimeExerciseTema3.Controllers
{
    public class CustomDateTimeController : Controller
    {
        CustomDateTimeRepository _repo;

        public CustomDateTimeController(CustomDateTimeRepository repo)
        {
            _repo = repo;
        }

        public IActionResult Index()
        {
            var date = _repo.Date;
            return View(date);
        }
    }
}